<div class="container center-all mt-5" id="container-main">
    <div class="row w-100">
        <div class="col-12 w-100">
            
        
        <table class="table table-success table-striped table-hover rounded">
            <thead>
                <h2>Elenco Utenti</h2>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Cognome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Operazioni</th>
                    </tr>
            </thead>
            <tbody>



                <?php

                    $utenti = leggi_utenti();
                    while ($utente = mysqli_fetch_assoc($utenti))
                    {
                        echo "
                            <tr>
                            
                                <td style='color: black;'>
                                    {$utente['id']}
                                </td>

                                <td style='color: black;'>
                                    {$utente['nome']}
                                </td>

                                <td style='color: black;'>
                                    {$utente['cognome']}
                                </td>

                                <td style='color: black;'>
                                    {$utente['email']}                                
                                </td>

                                <td 
                                    class='text-start'
                                    style='color: black;'>



                                    <div class='d-flex  p-0 justify-content-md-start justify-content-between '>


                                    <!-- tasto dettaglio --!>
                                    <form method='POST' action='detail.php'>
                                        <input name='id' type='text' class='d-none'
                                        value='{$utente['id']}'>
                                        <button name='detail' type='submit' class='p-0'   style='background-color: transparent; border: none;'>                                <i class='fa-regular fa-eye '></i>
                                        </button>
                                    </form>
                                   

                                    <!-- tasto edit --!>
                                    <form method='POST' action='update.php'>
                                        <input name='id' type='text' class='d-none'
                                                value='{$utente['id']}'>
                                        <button name='update' type='submit' class='p-0'   style='background-color: transparent; border: none;'>                                             <i class='fa-regular        fa-pen-to-square mx-3'></i>
                                        </button>
                                    </form>
                                      

                                    <!-- tasto cancella --!>
                                    <form method='POST' action='utenti.php'>
                                        <input name='id' type='text' class='d-none'
                                            value='{$utente['id']}'>
                                        <button name='delete' type='submit' class='p-0'   style='background-color: transparent; border: none;'>                                    
                                        <i class='fa-regular fa-circle-xmark '>
                                        </i>
                                        </button>
                                    </form>

                                    </div>
                                    </td>
                            
                            </tr>
                            ";
                        } 
                        
                    ?>

   

                </tbody>
            </table>



        </div>
    </div>
</div>