<!-- Section: Design Block -->
<section class="text-center text-lg-start">
  <style>
    .cascading-right {
      margin-right: -50px;
    }

    @media (max-width: 991.98px) {
      .cascading-right {
        margin-right: 0;
      }
    }
  </style>



<div class="container  my-5">
  <div class="row center-all">
    <div class="col-12 col-md-6 rounded" style="
            background: hsla(0, 0%, 100%, 0.55);
            backdrop-filter: blur(30px);  
            box-shadow: 5px 5px 15px rgba(0,0,0,0.5);"
            ">


      <h2 class="fw-bold  text-start mb-5 mt-1">Dettaglio utente</h2>

        <form action="detail.php" method="GET">
          <!-- riga nome e cognome -->
          <div class="row">
                <div class="col-md-6 mb-4">
                  <div class="form-outline">
                    <?php 
                      echo "<input 
                              value = {$utente['nome']}
                              name='nome' type='text' id='form3Example1' class='form-control' disabled />";
                    ?>

                    <label class="form-label" for="form3Example1">Nome</label>
                  </div>
                </div>
                <div class="col-md-6 mb-4">
                  <div class="form-outline">

                    <?php 
                      echo "<input 
                              value = {$utente['cognome']}
                              name='cognome' type='text' id='form3Example2' class='form-control' disabled />";
                    ?>


                    <label class="form-label" for="form3Example2">Cognome</label>
                  </div>
                </div>
          </div>

              <!-- Email input -->
              <div class="form-outline mb-4">


                    <?php 
                      echo "<input 
                              value = {$utente['email']}
                              name='email' type='email' id='form3Example3' class='form-control' disabled />";
                    ?>

                <label class="form-label" for="form3Example3">Indirizzo email</label>
              </div>
          

        </form>

    </div>  
  </div>
</div>


  
</section>
<!-- Section: Design Block -->