<?php
  if(isset($_SESSION['utente_loggato']))
  {
    $email_utente_loggato = $_SESSION['email'];
    $nome_utente_loggato = $_SESSION['nome'];
    $cognome_utente_loggato = $_SESSION['cognome'];
    $utente_loggato = true;
  }     
  else
  {
    $utente_loggato = false;
  }
?>


<div id="barra-header" class="" >
<div class="px-3 py-2 bg-dark text-white ">
      <div class="container ">
        <div class="d-flex flex-wrap align-items-center justify-content-end ">


        <?php
             
;


                if (

                  isset($_SESSION['messaggio']) && !$_SESSION['messaggio_mostrato']

                  // ( 
                  // isset($_POST['modifica']) || (isset($_POST['login'])) ||
                  // isset($_POST['registra'])
                  // ) 
                   )
                {
                  echo 
                  "
                    <span 
                    id='box_messaggio'
                    
                    class='d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none rounded bg-success ms-2 py-2 px-3'>{$_SESSION['messaggio']}</span>

                    

                  ";


                  
                }
                  





        ?>



                    <script>
                      e_box_messaggio = document.getElementById('box_messaggio');
                      console.log('elemento', e_box_messaggio);
                      setTimeout (disattiva_messaggio, 5000);
                      function disattiva_messaggio()
                      {
                        e_box_messaggio.classList.add('d-none');
                        <?php

                          if (isset($_SESSION['messaggio']) && !$_SESSION['messaggio_mostrato'])
                              {
                                $_SESSION['messaggio_mostrato'] = true;
                              }

                        ?>
                      }
                    </script>

          <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small ">
            <li>
              <a href="index.php" class="nav-link 
                <?php 
                  if($_SESSION['page'] == 'index')
                  {
                    echo "text-white";
                  }
                    
                  else  
                  {
                    echo "text-secondary";
                  }
                ?>                    
              ">
                <div class="d-flex flex-column center-all ">
                    <i class="fa-solid fa-house fa-2x center-all"></i>
                    Home
                </div>
              </a>
            </li>
            <li>
              <a href="#" class="nav-link center-all
              <?php 
                  if($_SESSION['page'] == 'dashboard')
                  {
                    echo "text-white";
                  }
                    
                  else  
                  {
                    echo "text-secondary";
                  }
                ?> 
              
              
              
              ">
              
                <div class="d-flex flex-column center-all ">
                    <i class="fa-solid fa-gauge fa-2x center-all"></i>
                    Dashboard
                </div>
                
              </a>
            </li>
            <li>
              <a href="#" class="nav-link 
              <?php 
                  if($_SESSION['page'] == 'orders')
                  {
                    echo "text-white";
                  }
                    
                  else  
                  {
                    echo "text-secondary";
                  }
                ?> 
              
              
              ">
                <div class="d-flex flex-column center-all ">
                    <i class="fa-regular fa-calendar-days fa-2x center-all"></i>
                    Orders
                </div>
              </a>
            </li>
            <li>
              <a href="#" class="nav-link
              <?php 
                  if($_SESSION['page'] == 'products')
                  {
                    echo "text-white";
                  }
                    
                  else  
                  {
                    echo "text-secondary";
                  }
                ?> 
              
              ">
                <div class="d-flex flex-column center-all ">
                
                    <i class="fa-solid fa-boxes-stacked fa-2x center-all"></i>
                        Products
                </div>              
              </a>
            </li>
            <li>
              <a href="utenti.php" class="nav-link 
              
              <?php 
                  if($_SESSION['page'] == 'utenti')
                  {
                    echo "text-white";
                  }
                    
                  else  
                  {
                    echo "text-secondary";
                  }
                ?> 
              
              
              ">
              <div class="d-flex flex-column center-all ">
                    <i class="fa-solid fa-circle-user fa-2x center-all"></i>
                    Utenti
                </div>
                
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="px-3 py-2 border-bottom mb-3 barra-login" >
          
    
            
      <div class="container d-flex flex-wrap justify-content-center">
        <form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
          <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>

        <?php         

        if ($utente_loggato)        {
            echo 
            "
              <div class='center-all'>
              <b>
                {$cognome_utente_loggato} {$nome_utente_loggato}
              </b>
              </div>
            ";
        }
        else
         {
          echo
              "
                <div class='text-end'>
                <a href='login.php'><button type='button' class='btn btn-light text-dark me-2'>Login</button></a>
                <a href='register.php'><button type='button' class='btn btn-primary'>Registrati</button></a>
                </div>            
              ";
         }
            
        ?>

      </div>
    </div>
    


<!-- chiude #barra-header -->
</div>


